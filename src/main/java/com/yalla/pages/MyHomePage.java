package com.yalla.pages;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyHomePage extends Annotations {
	
	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="(//*[text()='Create Lead'])[1]") WebElement elecreateLeadLink;
	@FindBy(how=How.XPATH, using="//a[text()='Leads']") WebElement eleLeadsLink;
	
	@And("Click on CreateLeads Link")
	public CreateLeadPage clickCreateLeads() {
		click(elecreateLeadLink);
		return new CreateLeadPage();
		
	}
	@And("Click on CreateLeads Tab")
	public MyLeadsPage clickLeadsTab() {		
		click(eleLeadsLink);
		return new MyLeadsPage();
		
	}

}
