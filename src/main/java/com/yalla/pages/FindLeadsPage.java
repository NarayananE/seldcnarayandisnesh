package com.yalla.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class FindLeadsPage extends Annotations {
	
	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="id") WebElement eleLeadId;
	@FindBy(linkText = "Find Leads") WebElement eleFindLeadsBtn;
	
	
	
	@And("Enter the Lead ID as (.*)")
	public void enterDealID(String leadId) {		
		clearAndType(eleLeadId, leadId);		
	}
	@And("Click on Find Lead Button")
	public void clickFindLeadBtn() {
		click(eleFindLeadsBtn);
	}
	@And("Click on the Resultant IDLink as (.*)")
	public void clickOnIDlink(String id) {
		click(driver.findElement(By.linkText(id)));
		
	}

}
