package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyLeadsPage extends Annotations {

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText = "Create Lead")
	WebElement eleCreatLead;
	@FindBy(linkText = "Find Leads")
	WebElement eleFindLeadsLink;

	public CreateLeadPage clickCreateLeadLink() {
		click(eleCreatLead);
		return new CreateLeadPage();
	}

	@And("Click on FindLeads Link")
	public FindLeadsPage clickFindLeasLink() {
		click(eleFindLeadsLink);
		return new FindLeadsPage();
	}

}
