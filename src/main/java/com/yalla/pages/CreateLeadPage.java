package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations {
	
	@FindBy(xpath="(//input[@name=\"companyName\"])[2]")
	WebElement eleCompanyName;
	@FindBy(xpath="(//input[@name=\"firstName\"])[3]")
	WebElement eleFirstName;
	@FindBy(xpath = "(//input[@name=\"lastName\"])[3]")
	WebElement eleLastName;
	@FindBy(xpath="//input[@name=\"submitButton\"]")
	WebElement eleSubmitBtn;
	
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@And("Enter Company Name as (.*)")
	public void enterCompanyName(String compName) {
		clearAndType(eleCompanyName, compName);
	}
	@And("Enter First Name as (.*)")
	public void enterFirstName(String fName) {
		clearAndType(eleFirstName, fName);
	}
	@And("Enter Last Name as (.*)")
	public void enterLastName(String lName) {
		clearAndType(eleLastName, lName);
	}
	@When("Click on Create Lead Button")
	public void clickSubmit() {
		click(eleSubmitBtn);
	}
	@Then("Verify the whether Lead Created or not")
	public void verifyLeadCreation() {
		System.out.println("The Lead has been created");
	}

}
