/*package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.CreateLeadPage;
import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyHomePage;
import com.yalla.pages.MyLeadsPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations {
	HomePage objHomePage;
	MyHomePage objMyHomePage;
	MyLeadsPage objMyLeadsPage;
	CreateLeadPage objCreateLeadPage;

	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Testing the Create Lead Functionality";
		author = "Narayanan E";
		category = "Functional";
		excelFileName = "TC001";
	}

	@Test()
	public void createLead() {
		LoginPage objLogin = new LoginPage();
		objLogin.enterUserName("DemoSalesManager");
		objLogin.enterPassWord("crmsfa");
		objHomePage = objLogin.clickLogin();
		objMyHomePage = objHomePage.clickCRMLink();
		objMyLeadsPage = objMyHomePage.clickLeads();
		objCreateLeadPage = objMyLeadsPage.clickCreateLeadLink();
		CreateLeadPage objCreateLeadPage = new LoginPage().enterUserName("DemoSalesManager")
		.enterPassWord("crmsfa")
		.clickLogin()
		.clickCRMLink()
		.clickLeads()
		.clickCreateLeadLink();
		objCreateLeadPage.enterCompanyName();
		objCreateLeadPage.enterFirstName();
		objCreateLeadPage.enterLastName();
		objCreateLeadPage.clickSubmit();
	}
}
*/