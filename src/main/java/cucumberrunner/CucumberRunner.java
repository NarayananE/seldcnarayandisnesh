package cucumberrunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
features = "src/main/java/featurefiles/editlead.feature",
glue = {"steps", "com.yalla.pages"},
monochrome = true,
dryRun = false,
snippets = SnippetType.CAMELCASE
/*tags = {"@smoke or @reg"}*/	)

public class CucumberRunner {

}
